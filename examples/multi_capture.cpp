
#include <camera.hpp>
#include <json.hpp>
#include <fstream>
#include <sstream>
#include <boost/program_options.hpp>

using namespace camera;
using namespace cv;
using namespace std;
using json = nlohmann::json;

struct CallBack {
  unsigned int save_every = 1 << 30;

  void operator()(const Frame& frame) {
    if ( (frame.num % save_every) == 0 ) {
      stringstream ss;
      ss << "c" << frame.cam_id << "_" << setfill('0') << setw(5) << frame.num << ".jpg";
      string fname;
      ss >> fname;
      imwrite(fname, frame.img);
    }
  }
};

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("save_every", po::value<unsigned int>(), "save only the frames multiple of the given number")
      ("conf,c", po::value<string>(), "path to the JSON configuration");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("conf")) {
      cerr << "Error: You should provide the JSON configuration file" << endl;
      return 1;
    }

    CallBack call_back;
    if (vm.count("save_every")) {
      call_back.save_every = vm.at("save_every").as<unsigned int>();
    }

    string conf_name = vm.at("conf").as<string>();
    ifstream in(conf_name);
    json jobj;
    in >> jobj;
    if (jobj.count("log")) set_log_config(jobj.at("log"));
    CameraSet cams(jobj.at("cameras"));
    cams.start(call_back);
    cout << "Capturing started. Type q to quit" << endl;
    char c;
    do {
      cin >> c;
    } while (c != 'q');
    cout << "Stopping" << endl;
    cams.stop();
    cout << "Quitting" << endl;
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
