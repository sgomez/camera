
#include <camera.hpp>
#include <json.hpp>
#include <fstream>
#include <sstream>
#include <boost/program_options.hpp>

using namespace camera;
using namespace cv;
using namespace std;
using json = nlohmann::json;

struct CallBack {
  unsigned int save_every = 500;
  VideoWriter outputVideo;
  unsigned int cam_id;

  CallBack(unsigned int cam_id) : cam_id(cam_id) {
    double out_fps = 30.0;
    Size out_size(493,659);
    stringstream ss;
    ss << "cam" << cam_id << ".mp4";
    string fname;
    ss >> fname;
    string codec = "avc1";
    /*int ex = CV_FOURCC(codec[0], codec[1], codec[2], codec[3]);
    outputVideo = VideoWriter(fname, ex, out_fps, out_size, true);
    if (!outputVideo.isOpened()) {
      cerr << "Could not open " << fname << endl;
      throw std::logic_error("Error opening output video");
    }*/
  }

  CallBack(const CallBack& other) {
    cerr << "Danger: attempting to copy the video object will fail id=" << other.cam_id << endl;
  }

  void operator()(const Frame& frame) {
    //Mat m = frame.img;
    //cout << frame.cam_id << "=" << cam_id << " n=" << frame.num << " " << m.size() << endl;
    //outputVideo << m;
    if ( (frame.num % save_every) == 0) {
      stringstream ss;
      ss << "c" << frame.cam_id << "_" << setfill('0') << setw(5) << frame.num << ".jpg";
      string fname;
      ss >> fname;
      imwrite(fname, frame.img);
    }
  }

  ~CallBack() {
    cout << "Flushing video of camera " << cam_id << endl;
    //outputVideo.release();
  }
};

VideoRec create_video_rec(unsigned int cam_id) {
  double out_fps = 30.0;
  Size out_size(659,493);
  stringstream ss;
  ss << "cam" << cam_id << ".mp4";
  string fname;
  ss >> fname;
  string codec = "avc1";
  VideoRec rec(fname, out_fps, out_size, codec, true);
  return rec;
}

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("conf,c", po::value<string>(), "path to the JSON configuration");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("conf")) {
      cerr << "Error: You should provide the JSON configuration file" << endl;
      return 1;
    }

    string conf_name = vm.at("conf").as<string>();
    ifstream in(conf_name);
    json jobj;
    in >> jobj;

    ThreadedListener listener;
    if (jobj.count("log")) set_log_config(jobj.at("log"));
    CameraSet cams(jobj.at("cameras"));
    vector<VideoRec> recorders;
    for (unsigned int i=0; i<cams.size(); i++) {
      recorders.push_back(create_video_rec(i+1));
      listener.add_listener(i+1, recorders.back());
    }
    cams.start(listener);
    cout << "Capturing started. Type q to quit" << endl;
    char c;
    do {
      cin >> c;
    } while (c != 'q');
    cout << "Stopping" << endl;
    cams.stop();
    cout << "Cameras stopped" << endl;
    listener.stop();
    cout << "Recording stopped" << endl;
    cout << "Quitting" << endl;
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
