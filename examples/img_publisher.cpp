
#include <camera.hpp>
#include <json.hpp>
#include <fstream>
#include <sstream>
#include <boost/program_options.hpp>
#include <zmqpp/zmqpp.hpp>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <queue>
#include <rob_proto/frame.pb.h>

using namespace camera;
using namespace cv;
using namespace std;
using json = nlohmann::json;

class ImgPub {
  private:
    class Impl;
    std::shared_ptr<Impl> _impl;
  public:
    ImgPub(const string& url);
    ~ImgPub() = default;
    void operator()(const Frame& frame);
    void stop();
};

class ImgPub::Impl {
  public:
    mutex mtx;
    condition_variable q_cv;
    deque<Frame> frames;
    thread srv_thread;
    bool running;

    void add_img(const Frame& frame) {
      std::unique_lock<mutex> lck(mtx);
      frames.push_back(frame);
      q_cv.notify_one();
    }

    void start_server(const string& url) {
      zmqpp::context ctx;
      zmqpp::socket pic_send(ctx, zmqpp::socket_type::pub);
      pic_send.bind( url );
      //ofstream tmp{"/local_data/sgomez/test.pb", ios::binary};

      running = true;
      while (running) {
        std::unique_lock<mutex> lck(mtx);
        while (frames.empty()) q_cv.wait(lck);

        Frame f = frames.front();
        frames.pop_front();
        lck.unlock();

        rob_proto::Frame frm;
        frm.set_cam_id(f.cam_id); 
        frm.set_num(f.num);
        frm.set_time(f.time.time_since_epoch().count());
        rob_proto::Image& img = *(frm.mutable_img());
        img.set_height(f.img.rows);
        img.set_width(f.img.cols);
        img.set_channels(f.img.channels());
        img.set_data((const char*)f.img.data, f.img.rows*f.img.cols*f.img.channels() );
        string ans = frm.SerializeAsString();
        /*unsigned int msg_size = ans.size();
        tmp.write((const char*)&msg_size, sizeof(msg_size));
        tmp.write(ans.data(), msg_size);*/

        zmqpp::message msg;
        msg << ans;
        pic_send.send(msg);
      }
      /*tmp.flush();
      tmp.close();*/
    }

    void stop() {
      std::unique_lock<mutex> lck(mtx);
      running = false;
      lck.unlock();
      srv_thread.join();
    }
};

ImgPub::ImgPub(const string& url) {
  _impl = shared_ptr<Impl>(new ImgPub::Impl());
  auto run = [this, url]() -> void {
    this->_impl->start_server(url);
  };
  _impl->srv_thread = thread(run);
}

void ImgPub::operator()(const Frame& f) {
  _impl->add_img(f);
}

void ImgPub::stop() {
  _impl->stop();
}

int main(int argc, char** argv) {
  try {
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help,h", "Produce help message")
      ("conf,c", po::value<string>(), "URL to create socket");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("conf")) {
      cerr << "Error: You should provide the JSON configuration file" << endl;
      return 1;
    }

    string conf_name = vm.at("conf").as<string>();
    ifstream in(conf_name);
    json jobj;
    in >> jobj;

    ImgPub call_back("tcp://*:7655");

    if (jobj.count("log")) set_log_config(jobj.at("log"));
    CameraSet cams(jobj.at("cameras"));
    cams.start(std::ref(call_back));
    cout << "Capturing started. Type q to quit" << endl;
    char c;
    do {
      cout << "q) Quit. Command: " << endl;
      cin >> c;
    } while (c != 'q');
    cout << "Stopping" << endl;
    call_back.stop();
    cams.stop();
    cout << "Quitting" << endl;
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }

}
