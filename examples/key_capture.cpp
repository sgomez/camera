
#include <camera.hpp>
#include <json.hpp>
#include <fstream>
#include <sstream>
#include <boost/program_options.hpp>
#include <mutex>

using namespace camera;
using namespace cv;
using namespace std;
using json = nlohmann::json;

class CallBack {
  private:
    long last_img;
    long save_img;
    long count;
    mutex mtx;

  public:

    CallBack() : last_img(-1), save_img(-1), count(0) {
    }

    void save_next() {
      unique_lock<mutex> lock(mtx);
      save_img = last_img + 1;
      count++;
    }

    void operator()(const Frame& frame) {
      unique_lock<mutex> lock(mtx);
      last_img = max(last_img, (long)frame.num); 
      if ( (long)frame.num == save_img ) {
        lock.unlock(); //no longer dangerous
        stringstream ss;
        ss << "cam" << frame.cam_id << "_" << setfill('0') << setw(8) << count << ".jpg";
        string fname;
        ss >> fname;
        imwrite(fname, frame.img);
      }
    }

    long get_index() const {
      return count;
    }

    void set_index(long index) {
      this->count = index;
    }
};

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("conf,c", po::value<string>(), "path to the JSON configuration")
      ("start_ix,s", po::value<long>(), "start index for the cameras");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("conf")) {
      cerr << "Error: You should provide the JSON configuration file" << endl;
      return 1;
    }

    CallBack call_back;

    if (vm.count("start_ix")){
      call_back.set_index(vm.at("start_ix").as<long>() - 1);
    }

    string conf_name = vm.at("conf").as<string>();
    ifstream in(conf_name);
    json jobj;
    in >> jobj;
    if (jobj.count("log")) set_log_config(jobj.at("log"));
    CameraSet cams(jobj.at("cameras"));
    cams.start(std::ref(call_back));
    cout << "Capturing started. Type q to quit" << endl;
    char c;
    do {
      cout << "q) Quit c) Capture. Command: " << endl;
      cin >> c;
      if (c == 'c') {
        call_back.save_next();
        cout << "Capturing images index " << call_back.get_index() << endl;
      }
    } while (c != 'q');
    cout << "Stopping" << endl;
    cams.stop();
    cout << "Quitting" << endl;
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
