
#include "camera.hpp"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <chrono>
#include <unordered_map>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace std;
using namespace cv;
using json = nlohmann::json;

namespace camera {

  class ThreadedListener::Impl {
    private:
      struct Queue {
        queue<Frame> q;
        condition_variable cv;
        mutex local;
        unsigned int warning_size;

        Frame pop() {
          unique_lock<mutex> lck(local);
          while(q.empty()) cv.wait(lck);
          Frame f = q.front();
          BOOST_LOG_TRIVIAL(debug) << "Popping frame size=" << f.img.size() << " from camera queue " << f.cam_id;
          q.pop();
          return f;
        }

        void push(const Frame& f) {
          unique_lock<mutex> lck(local);
          BOOST_LOG_TRIVIAL(debug) << "Pushing frame size=" << f.img.size() << " to camera queue " << f.cam_id;
          q.push(f);
          if (q.size() >= warning_size) {
            unsigned int new_warn_size = (unsigned int)(2 * warning_size);
            BOOST_LOG_TRIVIAL(warning) << "The queue size for camera " << f.cam_id << " is " << q.size() << 
              ". Updating warning size to " << new_warn_size;
            warning_size = new_warn_size;
          } else if ((4*q.size()) < warning_size && warning_size>5) {
            unsigned int new_warn_size = (unsigned int)(warning_size/2);
            BOOST_LOG_TRIVIAL(warning) << "The queue size for camera " << f.cam_id << " is " << q.size() << 
              ". Updating warning size to " << new_warn_size;
            warning_size = new_warn_size;
          }
          cv.notify_one();
        }

        Queue() : warning_size(4) {
        }
      };
    
      unordered_map<unsigned int, Queue> buffer;
      vector<std::thread> listeners;
      mutex general;
      bool running;
    public:

      Impl() {
        this->running = false;
      }

      ~Impl() {
        stop();
      }

      void add_listener(unsigned int cam_id, const CamListener& listener) {
        unique_lock<mutex> lck(general);
        Queue& q = buffer[cam_id];
        this->running = true;
        auto t_code = [&q, listener, this]() -> void {
          while (this->running) { 
            Frame f = q.pop();
            if (!this->running) break;
            listener(f);
          }
        };
        listeners.push_back(thread(t_code));
      }

      void add_frame(const Frame& frame) {
        unique_lock<mutex> lck(general);
        Queue& q = buffer[frame.cam_id];
        q.push(frame);
      }

      void stop() {
        unique_lock<mutex> lck(general);
        if (this->running) {
          this->running = false;
          //1) push empty frames to avoid waiting for ever
          auto ct = std::chrono::high_resolution_clock::now();
          Frame empty{0, 0, ct, Mat()};
          for (auto& p : buffer) {
            p.second.push(empty);
          }
          //2) wait until all threads are done
          for (auto& l : listeners) {
            l.join();
          }
          listeners.clear();
          BOOST_LOG_TRIVIAL(info) << "Threaded listener successfully stopped";
        }
      }
  };

  ThreadedListener::ThreadedListener() {
    _impl = shared_ptr<Impl>( new Impl() );
  }

  ThreadedListener::~ThreadedListener() = default;

  void ThreadedListener::add_listener(int cam_id, const CamListener& listener) {
    _impl->add_listener(cam_id, listener);
  }

  void ThreadedListener::operator()(const Frame& frame) {
    _impl->add_frame(frame);
  }

  void ThreadedListener::stop() {
    _impl->stop();
  }


  class VideoRec::Impl {
    private:
      unsigned int save_every;
      VideoWriter outputVideo;
      cv::Size frame_size;
      bool is_color;
    public:

      Impl(const std::string& fname, double fps, cv::Size frame_size, const std::string& codec, bool is_color) {
        int ex = CV_FOURCC(codec[0], codec[1], codec[2], codec[3]);
        outputVideo = VideoWriter(fname, ex, fps, frame_size, is_color);
        this->frame_size = frame_size;
        this->is_color = is_color;
        this->save_every = 20;
        if (!outputVideo.isOpened()) {
          BOOST_LOG_TRIVIAL(error) << "Could not open output video " << fname 
            << " with codec " << codec;
          throw std::logic_error("Could not open the output video");
        }
      }

      Impl(const Impl& other) = delete;

      void add_frame(const Frame& frame) {
        //if ((frame.num % save_every)==0) {
          int n_channels = is_color ? 3 : 1;
          if (frame.img.channels() != n_channels) {
            BOOST_LOG_TRIVIAL(error) << "Number of channels of the camera " << 
              frame.img.channels() << " does not match the video channels " << n_channels;
          } else {
            if (frame.img.size() != frame_size) {
              BOOST_LOG_TRIVIAL(debug) << "Re-sizing camera frame from " << frame.img.size() <<
                " to " << frame_size;
              Mat img;
              cv::resize(frame.img, img, frame_size);
              outputVideo << img;
            } else {
              outputVideo << frame.img;
            }
          }
        //}
      }

      ~Impl() {
        BOOST_LOG_TRIVIAL(debug) << "Video recorder destroyed" << endl;
      }
  };

  VideoRec::VideoRec(const string& file_name, double fps, cv::Size frame_size, const string& codec, bool is_color) {
    _impl = shared_ptr<Impl>(new Impl(file_name, fps, frame_size, codec, is_color));
  }

  VideoRec::~VideoRec() = default;

  void VideoRec::operator()(const Frame& frame) {
    _impl->add_frame(frame);
  }

};
