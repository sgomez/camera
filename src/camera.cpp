
#include "camera.hpp"
#include <arpa/inet.h>
#include <thread>
#include <vector>
#include <chrono>
#include <unordered_map>
#include <drivers/pvapi.hpp>
#include <drivers/opencv.hpp>
#include <drivers/raw_record.hpp>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace std;
using namespace cv;
using json = nlohmann::json;

namespace camera {

  using namespace drivers;

  class CameraSet::Impl {
    public:
      unique_ptr<CamSetDriver> cam_set;

      Impl(const json& cam) {
        BOOST_LOG_TRIVIAL(info) << "Creating a Camera Set object";
        const string& type = cam.at("type");
        BOOST_LOG_TRIVIAL(trace) << "Camera Set object driver: " << type;
        const json& conf = cam.at("conf");
        BOOST_LOG_TRIVIAL(trace) << "Camera Set object conf: " << conf;
        if (type == "pvapi") {
          cam_set = unique_ptr<CamSetDriver>(new PvAPI_CamSet(conf));
        } else if (type == "opencv") {
          cam_set = unique_ptr<CamSetDriver>(new OpenCV_CamSet(conf));
        } else if (type == "raw_rec") {
          cam_set = unique_ptr<CamSetDriver>(new RawRec_CamSet(conf));
        } else {
          throw std::logic_error("Unknown type of cameras to read from");
        }
        BOOST_LOG_TRIVIAL(trace) << "Camera Set object successfully created";
      }

      void start(const CamListener& listener) {
        cam_set->start(listener);
      }

      void stop() {
        cam_set->stop();
      }

      unsigned int size() const {
        return cam_set->size();
      }
  };

  CameraSet::CameraSet(const nlohmann::json& config) {
    _impl = shared_ptr<Impl>(new Impl(config));
  }

  CameraSet::CameraSet() {
    _impl = nullptr;
  }

  CameraSet::~CameraSet() = default;

  void CameraSet::start(const CamListener& listener) {
    _impl->start(listener);
  }

  void CameraSet::stop() {
    _impl->stop();
  }

  unsigned int CameraSet::size() const {
    return _impl->size();
  }

  void set_log_config(const json& log_conf) {
    bool auto_flush = false;
    if (log_conf.count("auto_flush")) auto_flush = log_conf.at("auto_flush");
    logging::add_common_attributes();
    logging::register_simple_formatter_factory< boost::log::trivial::severity_level, char >("Severity");
    if (log_conf.count("file")) {
      logging::add_file_log( 
          logging::keywords::file_name = log_conf.at("file").get<string>(), 
          logging::keywords::format = "[%TimeStamp%] (%LineID%) <%Severity%>: %Message%",
          logging::keywords::auto_flush = auto_flush);
    } else {
      logging::add_console_log(std::cout, 
          logging::keywords::format="%Message%",
          logging::keywords::auto_flush = auto_flush);
    }
    logging::trivial::severity_level logSeverity = logging::trivial::debug;
    if (log_conf.count("severity")) {
      const unordered_map<string,logging::trivial::severity_level> sevlev {
        {"trace", logging::trivial::trace},
        {"debug", logging::trivial::debug},
        {"info", logging::trivial::info},
        {"warning", logging::trivial::warning},
        {"error", logging::trivial::error},
        {"fatal", logging::trivial::fatal}};
      string sev = log_conf.at("severity");
      if (!sevlev.count(sev)) throw std::logic_error("Unrecognized severity level in the configuration");
      logSeverity = sevlev.at(sev);
    }
    logging::core::get()->set_filter(logging::trivial::severity >= logSeverity);
  }

};
