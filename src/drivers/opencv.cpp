
#include <drivers/opencv.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <vector>
#include <chrono>
#include <unordered_map>
#include <thread>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace std;
using namespace cv;
using json = nlohmann::json;

namespace camera {

  namespace drivers {

    class OpenCV_CamSet::Impl {
      private:
        vector<shared_ptr<VideoCapture>> cameras;
        vector<unsigned int> cam_ids;
        vector<thread> threads;
        vector<double> sleep_ms;
        bool running;
      public:

        Impl(const json& conf) : running(false) {
          for (auto& elem : conf) {
            unsigned int id = elem.at("ID");
            auto input = elem.at("input");
            cam_ids.push_back(id);
            sleep_ms.push_back(elem.at("sleep"));
            if (input.is_number_integer()) {
              unsigned int cam_id = input;
              cameras.push_back(make_shared<VideoCapture>(cam_id));
            } else if (input.is_string()) {
              string file_name = input;
              cameras.push_back(make_shared<VideoCapture>(file_name));
            } else {
              throw std::logic_error("The input property of the OpenCV driver "
                  "can be only an integer or a file name");
            }
          }
        }

        void start(const CamListener& listener) {
          running = true;
          auto method = [this](unsigned int cam_index, const CamListener& listener) {
            Frame frame;
            unsigned int cam_id = this->cam_ids[cam_index];
            auto cam = this->cameras[cam_index];
            std::chrono::duration<double,std::milli> sleep_dur(this->sleep_ms[cam_index]);
            for (unsigned int num=0; this->running; ++num) {
              (*cam) >> frame.img;
              if (frame.img.empty()) {
                BOOST_LOG_TRIVIAL(debug) << "End of video reached on camera " << cam_id << ". Exiting thread.";
                break;
              }
              frame.cam_id = cam_id;
              frame.num = num;
              frame.time = chrono::high_resolution_clock::now();
              BOOST_LOG_TRIVIAL(trace) << "Obtaining frame " << frame.num << " of camera " << cam_id << 
                " of size " << frame.img.size << ". Empty=" << frame.img.empty() << ", Total=" << frame.img.total();
              listener(frame);
              this_thread::sleep_for(sleep_dur);
            }
          };
          BOOST_LOG_TRIVIAL(debug) << "Starting the OpenCV video capture driver";
          for (unsigned int i=0; i<cameras.size(); ++i) {
            BOOST_LOG_TRIVIAL(debug) << "Launching thread for camera " << cam_ids[i];
            threads.push_back(thread(method,i,listener));
          }
        }

        void stop() {
          if (running) {
            running = false;
            BOOST_LOG_TRIVIAL(debug) << "Joining threads";
            for (auto& t: threads) {
              t.join();
            }
            BOOST_LOG_TRIVIAL(info) << "OpenCV camera capture stopped";
          }
        }

        unsigned int size() const {
          return cameras.size();
        }
        
    };

    OpenCV_CamSet::OpenCV_CamSet(const nlohmann::json& config) {
      _impl = unique_ptr<Impl>(new Impl(config));
    }

    void OpenCV_CamSet::start(const CamListener& listener) {
      _impl->start(listener);
    }
        
    void OpenCV_CamSet::stop() {
      _impl->stop();
    }

    unsigned int OpenCV_CamSet::size() const {
      return _impl->size();
    }

    OpenCV_CamSet::~OpenCV_CamSet() = default;

  };
};
