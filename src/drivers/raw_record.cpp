#include <drivers/raw_record.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <rob_proto/frame.pb.h>

#include <vector>
#include <fstream>
#include <chrono>
#include <unordered_map>
#include <thread>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace std;
using namespace cv;
using json = nlohmann::json;

namespace camera {

  namespace drivers {

    class RawRec_CamSet::Impl {
      private:
        ifstream in;
        thread forwarder;
        bool running;
        bool debayer;
        unsigned int num_cams;
        unsigned int delay_us;
      public:

        Impl(const json& conf) {
          BOOST_LOG_TRIVIAL(info) << "Creating raw recording forwarding";
          if (conf.count("file") + conf.count("num_cams") != 2) {
            BOOST_LOG_TRIVIAL(error) << "The raw recording driver requires a valid file and number of cameras";
            throw std::logic_error("Wrong JSON file format");
          }
          in = ifstream(conf.at("file"), std::ios::binary);
          BOOST_LOG_TRIVIAL(trace) << "raw recording file: " << conf.at("file");
          num_cams = conf.at("num_cams");
          BOOST_LOG_TRIVIAL(trace) << "raw recording num_cams: " << num_cams;
          debayer = ( (conf.count("debayer") != 0) && (((bool)conf.at("debayer")) == true) );
          BOOST_LOG_TRIVIAL(trace) << "debayer: " << debayer;
          delay_us = 0;
          if (conf.count("delay")) delay_us = conf.at("delay");
        }

        void start(const CamListener& listener) {
          running = true;
          BOOST_LOG_TRIVIAL(debug) << "Starting the raw recorded video forwarding driver";
          auto method = [this](const CamListener& listener) {
            Frame frame;
            unsigned int len;
            string buf;
            rob_proto::Frame pframe;
            std::chrono::microseconds delay{ this->delay_us };
            while ( running && in.read((char*)&len, sizeof(len)) ) {
              buf.resize(len);
              in.read(&buf[0], len);
              pframe.ParseFromString(buf);
              frame.cam_id = pframe.cam_id();
              frame.num = pframe.num();
              frame.time = chrono::high_resolution_clock::time_point(std::chrono::nanoseconds(pframe.time()));
              auto pimg = pframe.img();
              cv::Mat img;
              if (pframe.img().channels() == 1) {
                img = cv::Mat(pimg.height(), pimg.width(), CV_8UC1);
                std::copy(pimg.data().begin(), pimg.data().end(), img.data);
                if (debayer) {
                  cv::Mat color(pimg.height(), pimg.width(), CV_8UC3);
                  cv::cvtColor(img, color, CV_BayerBG2RGB);
                  img = color;
                }
              } else {
                img = cv::Mat(pimg.height(), pimg.width(), CV_8UC3);
                std::copy(pimg.data().begin(), pimg.data().end(), img.data);
              }
              frame.img = img;
              if (this->delay_us != 0)
                std::this_thread::sleep_for(delay);
              listener(frame);
            }
            running = false;
          };
          BOOST_LOG_TRIVIAL(debug) << "Stoping the raw recorded video forwarding driver";
          forwarder = thread(method, listener);
        }

        void stop() {
          if (running) {
            running = false;
            BOOST_LOG_TRIVIAL(debug) << "Joining threads";
            forwarder.join();
            BOOST_LOG_TRIVIAL(info) << "Raw recorded video forwarding stopped";
          }
        }

        unsigned int size() const {
          return num_cams;
        }
        
    };

    RawRec_CamSet::RawRec_CamSet(const nlohmann::json& config) {
      _impl = unique_ptr<Impl>(new Impl(config));
    }

    void RawRec_CamSet::start(const CamListener& listener) {
      _impl->start(listener);
    }
        
    void RawRec_CamSet::stop() {
      _impl->stop();
    }

    unsigned int RawRec_CamSet::size() const {
      return _impl->size();
    }

    RawRec_CamSet::~RawRec_CamSet() = default;

  };
};
