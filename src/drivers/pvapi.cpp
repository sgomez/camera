
#include <drivers/pvapi.hpp>
#include "PvApi.h"

#include <arpa/inet.h>
#include <thread>
#include <vector>
#include <chrono>
#include <unordered_map>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace std;
using namespace cv;
using json = nlohmann::json;

namespace camera {

  namespace drivers {

    struct PvAPI_Cam {
      unsigned int ID;
      unsigned long IP;
      tPvHandle Handle;
      vector<tPvFrame> Frames;
      unsigned long discarded;
      bool running;
      json conf;
#define FRAMESCOUNT 32

      bool CameraSetup() {
        tPvErr errCode;
        bool failed = false;
        unsigned long FrameSize = 0;
        BOOST_LOG_TRIVIAL(debug) << "Cam[" << ID << "]: Called Camera Setup";
        // Calculate frame buffer size
        if((errCode = PvAttrUint32Get(Handle,"TotalBytesPerFrame",&FrameSize)) != ePvErrSuccess)
        {
          BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: Get TotalBytesPerFrame err: " << errCode;
          return false;
        }

        BOOST_LOG_TRIVIAL(debug) << "Cam[" << ID << "]: Frame size " << FrameSize/1e6 << " MB" << endl;
        // allocate the frame buffers
        for(int i=0;i<FRAMESCOUNT && !failed;i++) {
          Frames.push_back(tPvFrame());
          Frames[i].ImageBuffer = new char[FrameSize];
          if(Frames[i].ImageBuffer) {
            Frames[i].ImageBufferSize = FrameSize;
          } else {
            BOOST_LOG_TRIVIAL(error) <<  "Cam[" << ID << "]: Failed to allocate buffers.";
            failed = true;
          }
        }
        BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: Buffer allocated";
        return !failed;
      }

      // close camera, free memory.
      void CameraUnsetup() {
        tPvErr errCode;

        // always close an opened camera even if it was unplugged before
        if((errCode = PvCameraClose(Handle)) != ePvErrSuccess) {
          BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvCameraClose err: " << errCode;
        } else {
          BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: Closed.";
        }
        // delete image buffers
        for(int i=0;i<FRAMESCOUNT;i++)
          delete [] (char*)Frames[i].ImageBuffer;

        Handle = NULL;
      }

      // setup and start streaming with PvCaptureWaitForFrameDone
      // return value: true == success (code exits on Ctrl+C abort), false == fail (other failure)
      bool CameraStart(const CamListener& listener) {
        tPvErr errCode;
        bool failed = false;
        int Index = 0;
        unsigned long Last = 0;

        // NOTE: This call sets camera PacketSize to largest sized test packet, up to 8228, that doesn't fail
        // on network card. Some MS VISTA network card drivers become unresponsive if test packet fails. 
        // Use PvUint32Set(handle, "PacketSize", MaxAllowablePacketSize) instead. See network card properties
        // for max allowable PacketSize/MTU/JumboFrameSize.
        if((errCode = PvCaptureAdjustPacketSize(Handle,conf.at("mtu"))) != ePvErrSuccess) {
          BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvCaptureAdjustPacketSize err: " << errCode;
          return false;
        }

        // start driver capture stream 
        if((errCode = PvCaptureStart(Handle)) != ePvErrSuccess)	{
          BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvCaptureStart err: " << errCode;
          return false;
        }

        // queue frames. No FrameDoneCB callback function.
        for(int i=0;i<FRAMESCOUNT && !failed;i++)
        {           
          if((errCode = PvCaptureQueueFrame(Handle,&(Frames[i]),NULL)) != ePvErrSuccess) {
            BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvCaptureQueueFrame err " << errCode;
            failed = true;
          }
        }

        if (failed) return false;

        if (conf.count("EnumSet")) {
          auto eset = conf.at("EnumSet");
          for (json::iterator es = eset.begin(); es != eset.end(); ++es) {
            string key = es.key(), value = es.value();
            if((PvAttrEnumSet(Handle,key.c_str(),value.c_str()) != ePvErrSuccess)) {
              BOOST_LOG_TRIVIAL(error) << "Failed to set property " << es.key() << " to " << es.value();
              return false;
            }
          }
        } else {
          BOOST_LOG_TRIVIAL(error) << "Missing configuration parameters of the kind EnumSet";
        }

        if (conf.count("Uint32Set")) {
          auto uset = conf.at("Uint32Set");
          for (json::iterator es = uset.begin(); es != uset.end(); ++es) {
            string key = es.key(); unsigned int value = es.value();
            if((PvAttrUint32Set(Handle,key.c_str(),value) != ePvErrSuccess)) {
              BOOST_LOG_TRIVIAL(error) << "Failed to set property " << es.key() << " to " << es.value();
              return false;
            }
          }
        }

        if (conf.count("Float32Set")) {
          auto fset = conf.at("Float32Set");
          for (json::iterator es = fset.begin(); es != fset.end(); ++es) {
            string key = es.key(); float value = es.value();
            if((PvAttrFloat32Set(Handle,key.c_str(),value) != ePvErrSuccess)) {
              BOOST_LOG_TRIVIAL(error) << "Failed to set property " << es.key() << " to " << es.value();
              return false;
            }
          }
        }

        if (conf.count("Sleep")) {
          this_thread::sleep_for(std::chrono::milliseconds(conf.at("Sleep")));
        }

        if ((PvCommandRun(Handle,"AcquisitionStart") != ePvErrSuccess)) {
          BOOST_LOG_TRIVIAL(error) << "Failed to start camera adquisition ID=" << ID;
          return false;
        }
        BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: Acquisition started";

        //Loop until failure or abortion
        while(!failed && running) {
          BOOST_LOG_TRIVIAL(debug) << "Cam[" << ID << "]: Running=" << running;
          //Wait for [Index] frame of FRAMESCOUNT num frames
          //wait for frame to return from camera to host
          while(!failed && running && 
              (errCode = PvCaptureWaitForFrameDone(Handle,&Frames[Index],conf.at("FrameTimeOut"))) == ePvErrTimeout) {
            BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: Timeout waiting for frame to return to host...";
          }
          if (!running) break;

          if(errCode != ePvErrSuccess) {
            BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: PvCaptureWaitForFrameDone err: " << errCode 
              << ". Is it unplugged?";
            failed = true;
          } else {
            tPvUint32 width, height;
            PvAttrUint32Get(Handle, "Width", &width);
            PvAttrUint32Get(Handle, "Height", &height);
            cv::Mat image(height, width, CV_8UC1, (uchar *) Frames[Index].ImageBuffer);
            Frame frame;
            frame.cam_id = ID;
            frame.num = Frames[Index].FrameCount;
            frame.time = chrono::high_resolution_clock::now();
            bool debayer = (!conf.count("debayer")) ? true : static_cast<bool>(conf.at("debayer"));
            if (debayer) { 
              cv::Mat color(height, width, CV_8UC3);
              cv::cvtColor(image, color, CV_BayerBG2RGB);
              frame.img = color;
            } else {
              frame.img = image;
            }
            BOOST_LOG_TRIVIAL(debug) << "Cam[" << ID << "]: Calling callback with image " << Frames[Index].FrameCount;
            listener(frame);
            // if frame hasn't been cancelled, requeue frame
            if(Frames[Index].Status != ePvErrCancelled) {
              if(Last + 1 != Frames[Index].FrameCount) {
                //Note missing frame
                discarded++;
              }

              Last = Frames[Index].FrameCount;

              //Requeue [Index] frame of FRAMESCOUNT num frames
              if ((errCode = PvCaptureQueueFrame(Handle,&Frames[Index],NULL)) != ePvErrSuccess) {
                BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvCaptureQueueFrame err " << errCode;
                failed = true;
              }

              //Increment [Index]
              Index++;
              if(Index==FRAMESCOUNT)
                Index = 0;        
            } else {
              //Cancelled
              failed = true;
            }
          }
        }
        return !failed;
      }

      // stop streaming, clear queue.
      void CameraStop() {
        tPvErr errCode;

        //stop camera receiving triggers
        if ((errCode = PvCommandRun(Handle,"AcquisitionStop")) != ePvErrSuccess) {
          BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: AcquisitionStop err %u" << errCode;
        } else {
          BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: AcquisitionStop success.";
          //Add delay between AcquisitionStop and PvCaptureQueueClear
          //to give actively written frame time to complete
          this_thread::sleep_for(std::chrono::milliseconds(200));
          BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: Calling PvCaptureQueueClear...";
          if ((errCode = PvCaptureQueueClear(Handle)) != ePvErrSuccess)
            BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvCaptureQueueClear err: " << errCode;
          else
            BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: ...Queue cleared.";

          //stop driver stream
          if ((errCode = PvCaptureEnd(Handle)) != ePvErrSuccess)
            BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvCaptureEnd err: %u" << errCode;
          else
            BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: Driver stream stopped.";
        }
      }

      void operator()(const CamListener& listener) {
        tPvErr errCode;
        running = true;
        if((errCode = PvCameraOpenByAddr(IP,ePvAccessMaster,&(Handle))) == ePvErrSuccess) {
          char sIP[128];
          char Name[128];
          // Read the IP and Name strings
          if(((errCode = PvAttrStringGet(Handle,"DeviceIPAddress",sIP,128,NULL)) == ePvErrSuccess) &&
              ((errCode = PvAttrStringGet(Handle,"CameraName",Name,128,NULL)) == ePvErrSuccess)) {
            BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: " << sIP << " (" << Name << ") opened";
            if(CameraSetup()) {
              CameraStart(listener);
              tPvFloat32 frame_rate, stat_frame_rate;
              PvAttrFloat32Get(Handle, "FrameRate", &frame_rate);
              PvAttrFloat32Get(Handle, "StatFrameRate", &stat_frame_rate);
              CameraStop();
              CameraUnsetup();
              BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: " << discarded << " frames missed by host frame queue";
              BOOST_LOG_TRIVIAL(info) << "Cam[" << ID << "]: Frame rate (set=" << frame_rate 
                << ", obtained=" << stat_frame_rate << ")";
            }
          }
          else {
            BOOST_LOG_TRIVIAL(error) << "Cam[" << ID << "]: PvAttrStringGet err: " << errCode;
          }
          PvCameraClose(Handle);
          Handle = 0;
        }
      }
    };

    class PvAPI_CamSet::Impl {
      public:
        vector<PvAPI_Cam> cameras;
        vector<thread> threads;

        Impl(const json& config) {
          tPvErr errCode;

          if ((errCode = PvInitialize()) != ePvErrSuccess) {
            BOOST_LOG_TRIVIAL(error) << "PvInitialize returned error: " << errCode;
            throw std::logic_error("Error on PvInitialize");
          }
          
          for (auto cam: config) {
            PvAPI_Cam pcam;
            pcam.ID = cam.at("ID");
            string ip = cam.at("IP");
            pcam.IP = inet_addr(ip.c_str());
            pcam.conf = cam.at("conf");
            pcam.running = false;
            if((pcam.IP == INADDR_NONE) || (pcam.IP == INADDR_ANY)) {
              BOOST_LOG_TRIVIAL(error) << "The IP address " << cam.at("IP") << " is not considered to be valid";
              throw std::logic_error("A valid IP address must be entered");
            }
            cameras.push_back(pcam);
          }
        }

        void start(const CamListener& listener) {
          for (auto &pcam : cameras) {
            threads.push_back( thread(std::ref(pcam), listener) );
          }
        }

        void stop() {
          for (auto& pcam : cameras) {
            pcam.running = false;
          }
          for (unsigned int i=0; i<threads.size(); i++) {
            BOOST_LOG_TRIVIAL(debug) << "Cam[" << cameras[i].ID << "]: joining thread. Running=" << cameras[i].running;
            threads[i].join();
            BOOST_LOG_TRIVIAL(debug) << "Cam[" << cameras[i].ID << "]: thread joined";
          }
          threads.clear();
        }

        unsigned int size() const {
          return cameras.size();
        }

        ~Impl() {
          stop();
          PvUnInitialize();
        }
    };

    PvAPI_CamSet::PvAPI_CamSet(const nlohmann::json& config) {
      _impl = unique_ptr<Impl>(new Impl(config));
    }

    void PvAPI_CamSet::start(const CamListener& listener) {
      _impl->start(listener);
    }
        
    void PvAPI_CamSet::stop() {
      _impl->stop();
    }
        
    unsigned int PvAPI_CamSet::size() const {
      return _impl->size();
    }
  
    PvAPI_CamSet::~PvAPI_CamSet() = default;

  };
};
