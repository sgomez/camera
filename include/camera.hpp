#ifndef ROB_CAM_HPP
#define ROB_CAM_HPP

/**
 * @file
 * Include this file to access the C++ API that can be used to obtain images
 * from the set of cameras.
 */

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "json.hpp"
#include <memory>
#include <functional>
#include <chrono>

namespace camera {

  struct Frame {
    unsigned int cam_id; //!< ID of the camera
    unsigned long num; //!< Counter that identifies the frame
    std::chrono::high_resolution_clock::time_point time; //!< Time stamp of the frame
    cv::Mat img; //!< Image adquired with the camera
  };

  /**
   * @brief Function type of the camera frame listeners. 
   *
   * This method will be called with all the frames collected by the 
   * camera. It is very important that the objects that implement 
   * this listener type can be safely copied.
   * Therefore, it is recommended to use the pointer-to-implementation idiom
   * with shared pointers.
   */
  using CamListener = std::function<void(const Frame& frame)>;

  class CamSetDriver {
    public:
      virtual void start(const CamListener& listener) = 0;
      virtual void stop() = 0;
      virtual unsigned int size() const = 0;
      virtual ~CamSetDriver() = default;
  };

  class CameraSet {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      CameraSet();
      CameraSet(const nlohmann::json& config);
      ~CameraSet();
      void start(const CamListener& listener);
      void stop();
      unsigned int size() const; //!< Number of cameras
  };

  /**
   * @brief Sets the log configuration
   */
  void set_log_config(const nlohmann::json& conf);

  /**
   * @brief Objects of this class are callable. They maintain multiple processing 
   * threads of listeners, queuing frames when no thread is available.
   */
  class ThreadedListener {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      ThreadedListener();
      ~ThreadedListener();
      void add_listener(int cam_id, const CamListener& listener);
      void operator()(const Frame& frame);
      void stop();
  };

  /**
   * @brief Callable object that saves all the frames passed to it on a
   * video in disk.
   */
  class VideoRec {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      VideoRec(const std::string& file_name, double fps, cv::Size frame_size, const std::string& codec, bool is_color);
      ~VideoRec();
      void operator()(const Frame& frame);
  };

};

#endif
