#ifndef ROB_CAM_DRIVER_OCV_HPP
#define ROB_CAM_DRIVER_OCV_HPP

#include <memory>
#include <camera.hpp>
#include <json.hpp>

namespace camera {

  namespace drivers {

    class OpenCV_CamSet : public CamSetDriver {
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
      public:
        OpenCV_CamSet(const nlohmann::json& config);
        void start(const CamListener& listener);
        void stop();
        unsigned int size() const;
        ~OpenCV_CamSet();
    };

  };
  
};

#endif
