#ifndef ROB_CAM_PVAPI_HPP
#define ROB_CAM_PVAPI_HPP

#include <memory>
#include <camera.hpp>
#include <json.hpp>

namespace camera {

  namespace drivers {

    class PvAPI_CamSet : public CamSetDriver {
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
      public:
        PvAPI_CamSet(const nlohmann::json& config);
        void start(const CamListener& listener);
        void stop();
        unsigned int size() const;
        ~PvAPI_CamSet();
    };

  };
  
};

#endif
