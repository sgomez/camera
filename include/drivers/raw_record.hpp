#ifndef ROB_CAM_DRIVER_RAWR_HPP
#define ROB_CAM_DRIVER_RAWR_HPP

#include <memory>
#include <camera.hpp>
#include <json.hpp>

namespace camera {

  namespace drivers {

    class RawRec_CamSet : public CamSetDriver {
      private:
        class Impl;
        std::unique_ptr<Impl> _impl;
      public:
        RawRec_CamSet(const nlohmann::json& config);
        void start(const CamListener& listener);
        void stop();
        unsigned int size() const;
        ~RawRec_CamSet();
    };

  };
  
};

#endif
