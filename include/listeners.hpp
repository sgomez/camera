#ifndef ROB_CAM_LISTENER_HPP
#define ROB_CAM_LISTENER_HPP

/**
 * @file
 * Include this file to access some pre-defined listeners or listener helpers
 */

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "json.hpp"
#include <memory>
#include <functional>
#include "camera.hpp"

namespace camera {


};

#endif
